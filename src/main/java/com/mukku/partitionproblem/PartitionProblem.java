/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mukku.partitionproblem;

import java.util.ArrayList;

/**
 *
 * @author acer
 */
public class PartitionProblem {

    public static void main(String[] args) {
        int S[] = {3, 1, 1, 2, 2, 1, 4};
//        int S[] = {1, 5, 11, 5};
//        int S[] = {13, 5, 6};
//        int S[] = {3, 1, 1};
//        int S[] = {};
//        int S[] = {3};
//        int S[] = {3, 1};
//        int S[] = {1, 1};
        int n = S.length;
        if (canPartition(S, n)) {
            System.out.println("Can be partitioned");
        } else {
            System.out.println("Cannot be partitioned");
        }

    }

    private static boolean canPartition(int[] S, int n) {
        if (n == 0 || n == 1) {
            return false;
        }
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += S[i];
        }
//        System.out.println(sum);
        if (sum % 2 == 0) {
            int sumN = 0;
            int subSum = sum / 2;
            return canDivided(S, subSum, 0, sumN);
        }
        return false;
    }

    private static boolean canDivided(int[] S, int tgSum, int nowI, int sumN) {
//        for (int i : S) {
//            System.out.print(i + " ");
//        }
//        System.out.println(" " + tgSum + " " + nowI + " " + S[nowI] + " " + sumN);
//        System.out.println();

        if (sumN == tgSum) {
            return true;
        }
        if (nowI == S.length - 1 && sumN != tgSum) {
            return false;
        }
        return canDivided(S, tgSum, nowI + 1, sumN) || canDivided(S, tgSum, nowI + 1, sumN + S[nowI + 1]);
                                           //don't add next index or add next index in subset ; check with target sum
    }

//    private static boolean canDivided(int[] S, int sum) {
//        int sum1 = 0;
//        int sum2 = 0;
//        ArrayList S1 = new ArrayList();
//        ArrayList S2 = new ArrayList();
//        for (int i = 0; i < S.length; i++) {
//            if (sum1 + S[i] <= sum) {
//                sum1 += S[i];
//                S1.add(S[i]);
//            } else {
//                sum2 += S[i];
//                S2.add(S[i]);
//            }
//        }
//        if (sum1 == sum2) {
//            System.out.println("This can divide as " + S1 + " and " + S2);
//            return true;
//        }
//        return false;
//    }
}
